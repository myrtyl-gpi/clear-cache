# Bitbucket Pipelines Pipe: Clear Cache Pipe

This pipe lets you clear all or selected Bitbucket Pipelines caches of your repository. 

By default this will clear all dependency caches that exist for this repository, but it can also be configured to clear specific caches. 

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: atlassian/clear-cache:0.1.2
    variables:
      BITBUCKET_USER: "<string>"
      BITBUCKET_PASSWORD: "<string>"
      # CACHES: "[<string>]"  # Optional
      # DEBUG: "<boolean>" # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| BITBUCKET_USER (*)    | The Bitbucket username that matches the BITBUCKET_PASSWORD  |
| BITBUCKET_PASSWORD (*)| A Bitbucket App Password                                    |
| CACHES                | An optional list of names of caches that should be cleared  |
| DEBUG                 | Turn on extra debug information. Default: `false`.          |

_(*) = required variable._

## Prerequisites

This pipe requires Bitbucket credentials, so the pipe can communicate with the Bitbucket API to retrieve and clear dependency caches. 
Specifically, a Bitbucket username and corresponding App password are required.  

Details on how to generate an App password can be found here: [https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html](https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html)

When generating an App password, make sure you give it the following scopes: 

* pipelines:read
* pipelines:write


## Examples

Basic example (clearing all caches):

```yaml
script:
  - pipe: atlassian/clear-cache:0.1.2
    variables:
      BITBUCKET_USER: "username"
      BITBUCKET_PASSWORD: "password"
```

Advanced example (clearing specific caches):

```yaml
script:
  - pipe: atlassian/clear-cache:0.1.2
    variables:
      BITBUCKET_USER: "username"
      BITBUCKET_PASSWORD: "password"
      CACHES: ["maven", "node"]
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community](https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes).

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
