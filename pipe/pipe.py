import requests
from requests.auth import HTTPBasicAuth

from bitbucket_pipes_toolkit import Pipe


schema = {
    'BITBUCKET_REPO_OWNER_UUID': {'required': True, 'type': 'string'},
    'BITBUCKET_REPO_SLUG': {'required': True, 'type': 'string'},
    'BITBUCKET_USER': {'required': True, 'type': 'string'},
    'BITBUCKET_PASSWORD': {'required': True, 'type': 'string'},
    'CACHES_COUNT': {'required': False, 'type': 'integer', 'default': 0},
    'CACHES': {'required': True, 'type': 'list'}
}

pipe = Pipe(schema=schema)

def run_pipe():
    pipe.log_info('Executing the pipe...')
    account_uuid = pipe.get_variable('BITBUCKET_REPO_OWNER_UUID')
    repo_uuid = pipe.get_variable('BITBUCKET_REPO_SLUG')
    bitbucket_user = pipe.get_variable('BITBUCKET_USER')
    bitbucket_password = pipe.get_variable('BITBUCKET_PASSWORD')
    caches_count = pipe.get_variable('CACHES_COUNT')

    caches_to_clear = pipe.get_variable('CACHES')[:caches_count]

    url = 'https://api.bitbucket.org/internal/repositories/{' + account_uuid + '}/' + repo_uuid + '/pipelines_caches/?page=1&pagelen=100'
    auth = HTTPBasicAuth(bitbucket_user, bitbucket_password)

    response = requests.get(url, auth=auth)
    if not response.ok:
        pipe.fail(f'Failed to retrieve caches: {response.status_code} {response.json()} {response.request.url}')
    pipe.log_debug(response.request.headers)
    pipe.log_debug(response.content)
    response_json = response.json()
    if not caches_to_clear:
        clear_all_caches(account_uuid=account_uuid, repo_uuid=repo_uuid,
                       cache_list_json=response_json["values"], auth=auth)
    else:
        clear_selected_caches(account_uuid=account_uuid, repo_uuid=repo_uuid,
                            cache_list_json=response_json["values"], caches_to_clear=caches_to_clear, auth=auth)

    pipe.success('Finished clearing caches')


def clear_all_caches(account_uuid, repo_uuid, cache_list_json, auth):
    pipe.success('Retrieved {} caches'.format(len(cache_list_json)))
    for cache in cache_list_json:
        pipe.log_debug(cache)
        clear_cache_by_uuid(account_uuid=account_uuid, repo_uuid=repo_uuid,
                         cacheUuid=cache["uuid"], cache_name=cache["name"], auth=auth)


def clear_selected_caches(account_uuid, repo_uuid, cache_list_json, caches_to_clear, auth):
    for cache in cache_list_json:
        pipe.log_debug(cache)
        if cache["name"] in caches_to_clear:
            clear_cache_by_uuid(account_uuid=account_uuid, repo_uuid=repo_uuid,
                             cacheUuid=cache["uuid"], cache_name=cache["name"], auth=auth)


def clear_cache_by_uuid(account_uuid, repo_uuid, cacheUuid, cache_name, auth):
    delte_url = 'https://api.bitbucket.org/internal/repositories/{' + \
        account_uuid + '}/' + repo_uuid + '/pipelines_caches/' + cacheUuid
    delete_response = requests.delete(delte_url, auth=auth)

    if delete_response.ok:
        pipe.success('Successfully cleared cache {}'.format(cache_name))
    else:
        response_json = delete_response.json()
        pipe.fail(f"Failed to clear cache {cache_name}: {response_json}")


if __name__ == '__main__':
    run_pipe()
