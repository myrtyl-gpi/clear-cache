# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.2

- patch: Updated the internal library version

## 0.1.1

- patch: Improve documentation and release process

## 0.1.0

- minor: Initial release

